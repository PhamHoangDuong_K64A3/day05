<?php
$genders = array(0 => "Nam", 1 => "Nữ");
$faculties = array("" => "", "MAT" => "Khoa học máy tính", "KDL" => "Khoa học vật liệu");
$warning = [""];

if (!empty($_POST["submit"])) {
    if (empty($_POST["name"])) {
        array_push($warning, "Hãy nhập tên.");
    }
    if (!isset($_POST["gender"])) {
        array_push($warning, "Hãy chọn giới tính.");
    }
    if (empty($_POST["faculty"])) {
        array_push($warning, "Hãy chọn phân khoa.");
    }
    if (empty($_POST["birthday"])) {
        array_push($warning, "Hãy nhập ngày sinh.");
    } elseif (check_date($_POST["birthday"]) == false) {
        array_push($warning, "Hãy nhập ngày sinh đúng định dạng.");
    }
    $file_name = pathinfo($_FILES["picture"]["name"])["filename"];
    $extension = pathinfo($_FILES["picture"]["name"])["extension"];
    $file_path = "upload/" . $file_name . "_" . date("YmdHis") . "." . $extension;
    if ($_FILES["picture"]["error"] == 0) {
        if (exif_imagetype($_FILES["picture"]["tmp_name"]) == false) {
            array_push($warning, "File đã chọn không phải hình ảnh.");
        }
        move_uploaded_file($_FILES["picture"]["tmp_name"], $file_path);
        $_POST["picture"] = $file_path;
    } else {
        $_POST["picture"] = "images/blank.png";
    }
    if ($warning == [""]) {
        session_start();
        $_SESSION = $_POST;
        session_write_close();
        header("Location: confirm.php");
    }
}

function check_date($date_str, $format = 'd/m/Y')
{
    $date = DateTime::createFromFormat($format, $date_str);
    return $date && $date->format($format) == $date_str;
}

$required = "<span style='color: #ff0000;'>*</span>";

?>

<html>

<head>
    <meta charset="UTF-8">
    <title>day05</title>
    <link rel="stylesheet" href="style.css">
</head>

<body>
    <div class="container">
        <?php foreach ($warning as $value) : ?>
            <p class="warning-msg"> <?= $value; ?> </p>
        <?php endforeach; ?>
        <form method="post" class="form-wrapper" enctype="multipart/form-data">
            <div>
                <label for="name" class="form-label">Họ và tên <?= $required; ?></label>
                <input type="text" name="name" id="name" class="form-input">
            </div>
            <div>
                <label for="gender" class="form-label">Giới tính <?= $required; ?></label>
                <?php
                for ($i = 0; $i < 2; $i++) {
                    echo "<input type='radio' id=$i name='gender' value=$i>
                         <label for=$i>$genders[$i]</label>";
                }
                ?>
            </div>
            <div class="select-wrapper">
                <label for="faculty" class="form-label">Phân khoa <?= $required; ?></label>
                <span class="select-arrow"></span>
                <select name="faculty" id="faculty" class="form-select">
                    <?php foreach ($faculties as $key => $value) : ?>
                        <option value=<?= $key; ?>><?= $value; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div>
                <label for="birthday" class="form-label">Ngày sinh <?= $required; ?></label>
                <input type="text" name="birthday" id="birthday" class="form-input date-picker" placeholder="dd/mm/yyyy" />
            </div>
            <div>
                <label for="address" class="form-label">Địa chỉ</label>
                <input type="text" name="address" id="address" class="form-input">
            </div>
            <div>
                <label for="picture" class="form-label">Hình ảnh</label>
                <input type="file" name="picture" id="picture">
            </div>
            <input type="submit" value="Đăng ký" name="submit" class="submit-btn" />
        </form>
    </div>

    <script>
        $(".datepicker").datepicker({
            dateFormat: "dd/mm/yyyy"
        });
    </script>

</body>

</html>
